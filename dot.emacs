;;; Symlink this file as ~/.emacs
;;; Alter occurrences of ~/Library/Emacs appropriately
;;; byte-recompile-directory as necessary

;(byte-recompile-directory "~/Library/Emacs/elisp" 0)
;(byte-recompile-directory "~/Library/Emacs/conf" 0)

(setq package-archives '(("elpa" . "http://tromey.com/elpa/")
                         ("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

(setq package-list
      '(
        dtrt-indent
        flymake-easy
        flymake-jshint
        point-stack
        rainbow-delimiters
        yaml-mode
        php-mode
        markdown-mode
        css-mode
        less-css-mode
        gist
        zoom-frm
        )
      )


(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

(setq default-directory "~/")

(setq load-path (cons "~/Library/Emacs/conf/" load-path))
(load-library "conf-main")

;;; Autosave just pisses me off
;; '(auto-save-visited-file-name t)
;; '(mac-command-modifier (quote meta) t)
;; '(mac-option-modifier nil t)
;; '(mac-pass-command-to-system t t)
;; '(mac-pass-option-to-system t t)

(add-hook 'find-file-hooks
  '(lambda ()
;;  (setq mode-line-buffer-identification 'buffer-file-truename)))
	 (setq mode-line-buffer-identification '("%32b"))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(*try-oblique-before-italic-fonts* t)
 '(auto-revert-interval 300)
 '(auto-revert-stop-on-user-input t)
 '(auto-save-file-name-transforms
   (quote
    ((".*" "~/Library/Emacs/autosave/" t)
     ("\\`/[^/]*:\\([^/]*/\\)*\\([^/]*\\)\\'" "/var/folders/f2/f2aBP+AcFVy8pjJJEede0U+++TI/-Tmp-/\\2" t))))
 '(auto-save-list-file-prefix "~/Library/Emacs/autosave/")
 '(c-block-comment-prefix "* ")
 '(c-cleanup-list (quote (scope-operator)))
 '(c-comment-continuation-stars "* " t)
 '(c-default-style "gnu4")
 '(c-indent-comments-syntactically-p nil)
 '(c-progress-interval 5)
 '(case-fold-search nil)
 '(column-number-mode t)
 '(compilation-always-signal-completion t)
 '(completion-ignored-extensions
   (quote
    ("CVS/" ".o" ".elc" "~" ".bin" ".lbin" ".fasl" ".dvi" ".toc" ".log" ".aux" ".a" ".so" ".la" ".ln" ".lof" ".blg" ".bbl" ".glo" ".idx" ".lot" ".fmt" ".diff" ".oi" ".class" ".jar" ".zip" ".core" ".jar")))
 '(confirm-kill-emacs (quote y-or-n-p))
 '(css-color-global-mode t)
 '(css-indent-offset 2)
 '(delete-key-deletes-forward t)
 '(delete-old-versions nil)
 '(delete-selection-mode t nil (delsel))
 '(efs-ftp-program-args (quote ("-i" "-n" "-g" "-v" "-p")))
 '(efs-use-passive-mode t)
 '(efs-use-remote-shell-internally nil)
 '(fast-lock-cache-directories (quote ("~/Library/Emacs/fastlock")))
 '(fill-column 74)
 '(find-file-compare-truenames t)
 '(find-file-use-truenames nil)
 '(font-lock-keywords-case-fold-search nil t)
 '(font-lock-maximum-decoration t)
 '(font-lock-use-colors (quote (color)))
 '(font-menu-ignore-scaled-fonts nil)
 '(font-menu-this-frame-only-p nil)
 '(frame-background-mode (quote dark))
 '(fume-max-items 32)
 '(fume-menubar-menu-location nil)
 '(global-auto-revert-mode nil nil (autorevert))
 '(global-font-lock-mode t nil (font-lock))
 '(global-whitespace-mode t)
 '(global-whitespace-newline-mode t)
 '(gnuserv-frame t)
 '(highlight-changes-global-initial-state (quote active))
 '(html-helper-use-expert-menu t)
 '(inhibit-startup-screen t)
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(isearch (quote isearch) t)
 '(isearch-lazy-highlight nil)
 '(kept-new-versions 5)
 '(kept-old-versions 5)
 '(less-compile-at-save nil)
 '(less-mode-hook nil)
 '(line-move-visual nil)
 '(mac-pass-command-to-system nil)
 '(mac-pass-control-to-system nil)
 '(mac-pass-option-to-system nil t)
 '(mumamo-chunk-coloring 99)
 '(mwheel-follow-mouse t)
 '(mwheel-scroll-amount (quote (1 . 5)))
 '(ns-alternate-modifier (quote hyper))
 '(ns-command-modifier (quote meta))
 '(ns-extended-platform-support-mode nil)
 '(ns-pop-up-frames nil)
 '(outline-mac-style t)
; '(package-archives
;   (quote
;    (("melpa" . "http://melpa.org/packages/")
;     ("gnu" . "http://elpa.gnu.org/packages/")
;     ("marmalade" . "http://marmalade-repo.org/packages/"))))
; '(package-get-always-update t)
; '(package-get-base-filename "/home/root/tmp/package-index.LATEST.pgp")
; '(package-get-dir "/home/root/tmp")
; '(package-get-remote
;   (quote
;    (("ftp.mirror.ac.uk" "sites/ftp.xemacs.org/pub/xemacs/packages"))))
 '(pandoc-binary "/usr/local/bin/pandoc")
 '(paren-mode (quote sexp) nil (paren))
 '(prolog-program-name "gprolog")
 '(query-user-mail-address nil)
 '(recent-files-save-file "~/Library/Emacs/recent-files.el")
 '(safe-local-variable-values
   (quote
    ((indent-tab-mode . t)
     (css-indent-offset . 4)
     (c-hanging-comment-ender-p))))
 '(save-completions-file-name "~/Library/Emacs/completions")
 '(shadow-info-file "~/Library/Emacs/shadows")
 '(shadow-noquery t)
 '(shadow-todo-file "~/Library/Emacs/shadow_todo")
 '(size-indication-mode t)
 '(special-display-regexps nil)
 '(sql-electric-stuff nil)
 '(ssl-certificate-directory "~/Library/Emacs/certs/")
 '(tab-width 4)
 '(teach-extended-commands-timeout 2)
 '(tool-bar-mode nil)
 '(toolbar-captioned-p nil)
 '(toolbar-visible-p nil)
 '(track-eol t)
 '(tramp-auto-save-directory "~/Library/Emacs/autosave/")
 '(url-automatic-caching t)
 '(url-be-asynchronous t)
 '(url-keep-history t)
 '(url-privacy-level (quote (os lastloc agent)))
 '(vc-cvs-header (quote ("$Header$")))
 '(vc-cvs-stay-local nil)
 '(vc-keep-workfiles nil)
 '(vc-mistrust-permissions t)
 '(version-control (quote never))
 '(whitespace-action (quote (report-on-bogus)))
 '(whitespace-line-column 80)
 '(whitespace-style
   (quote
    (face tabs spaces trailing space-before-tab newline empty space-after-tab)))
 '(xsl-indent-attributes t)
 '(zmacs-regions t))


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#111122" :foreground "#aaaabb" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 150 :width normal :foundry "apple" :family "monaco"))))
 '(bold ((t (:bold t))))
 '(bold-italic ((t (:size "12pt" :family "Courier" :bold t :italic t))))
 '(cperl-array ((((class color) (background dark)) (:foreground "Lightblue" :bold t))))
 '(cperl-hash ((((class color) (background dark)) (:foreground "pink" :bold t :italic t))))
 '(cperl-nonoverridable ((((class color) (background dark)) (:foreground "cornflowerblue"))))
 '(cursor ((t (:background "yellow" :foreground "white" :inverse-video t))))
 '(custom-group-tag ((((class color) (background dark)) (:foreground "#ffffcc" :underline t))))
 '(custom-state ((((class color) (background dark)) (:foreground "#9898cc"))))
 '(custom-variable-tag ((((class color) (background dark)) (:foreground "#ffffff" :underline t))))
 '(display-time-mail-balloon-enhance-face ((t (:foreground "#000000" :background "orange"))))
 '(fixed-pitch ((t (:family "lucida sans typewriter"))))
 '(font-lock-comment-face ((((class color) (background dark)) (:foreground "chocolate1"))))
 '(font-lock-string-face ((((class color) (min-colors 88) (background dark)) (:foreground "white"))))
 '(highlight ((t (:foreground "yellow" :background "black"))))
 '(highlight-changes ((((min-colors 88) (class color)) (:background "black"))))
 '(highlight-changes-delete ((((min-colors 88) (class color)) (:background "black"))))
 '(hl-buffer ((t (:background "black"))))
 '(html-helper-bold-face ((t (:foreground "#ffffa9"))))
 '(html-helper-italic-face ((t (:foreground "#a9ffff"))))
 '(isearch ((t (:foreground "black" :background "yellow"))))
 '(italic ((t (:foreground "red" :bold t))))
 '(list-mode-item-selected ((t (:foreground "#000000" :background "#689898"))) t)
 '(mode-line ((t (:foreground "black" :background "Grey80"))))
 '(nxml-attribute-local-name-face ((t (:inherit font-lock-variable-name-face))))
 '(nxml-cdata-section-content-face ((t (:inherit nxml-text-face :background "black" :foreground "white"))))
 '(nxml-comment-content-face ((t (:inherit font-lock-comment-face))))
 '(nxml-comment-delimiter-face ((t (:inherit nxml-comment-content-face))))
 '(nxml-delimited-data-face ((((class color) (background dark)) (:inherit font-lock-string-face))))
 '(nxml-delimiter-face ((((class color) (background dark)) (:inherit font-lock-builtin-face))))
 '(nxml-element-local-name-face ((t (:inherit nxml-name-face))))
 '(nxml-name-face ((((class color) (background dark)) (:inherit font-lock-function-name-face))))
 '(nxml-processing-instruction-delimiter-face ((t (:inherit font-lock-preprocessor-face))))
 '(nxml-text-face ((t (:inherit default))) t)
 '(paren-match ((t (:background "#004400"))) t)
 '(paren-mismatch ((t (:background "DeepPink"))) t)
 '(primary-selection ((t (:background "blue"))) t)
 '(rainbow-delimiters-depth-1-face ((t (:background "#440000"))))
 '(rainbow-delimiters-depth-2-face ((t (:background "#444400"))))
 '(rainbow-delimiters-depth-3-face ((t (:background "#004400"))))
 '(rainbow-delimiters-depth-4-face ((t (:background "#004444"))))
 '(rainbow-delimiters-depth-5-face ((t (:background "#000055"))))
 '(rainbow-delimiters-depth-6-face ((t (:background "#440044"))))
 '(rainbow-delimiters-depth-7-face ((t (:background "#442200"))))
 '(rainbow-delimiters-depth-8-face ((t (:background "#224400"))))
 '(rainbow-delimiters-depth-9-face ((t (:background "#220044"))))
 '(rainbow-delimiters-unmatched-face ((t (:background "#44444"))))
 '(secondary-selection ((t (:background "#986498"))))
 '(whitespace-empty ((t (:background "#000033"))))
 '(whitespace-hspace ((((class color) (background dark)) (:background "#000066"))))
 '(whitespace-indentation ((t (:background "#301010"))))
 '(whitespace-space ((((class color) (background dark)) (:background "#103010"))))
 '(whitespace-space-after-tab ((t (:background "#103010"))))
 '(whitespace-space-before-tab ((t (:background "#403000"))))
 '(whitespace-tab ((((class color) (background dark)) (:background "#301010"))))
 '(whitespace-trailing ((t (:background "#503000" :foreground "yellow" :weight bold))))
 '(widget-documentation ((((class color) (background dark)) (:foreground "#9898cc"))))
 '(widget-field ((((class grayscale color) (background dark)) (:foreground "grey20" :background "gray85"))))
 '(xsl-fo-alternate-face ((((background dark)) (:foreground "#ffffcc"))))
 '(xsl-fo-main-face ((((background dark)) (:foreground "#ffcccc"))))
 '(xsl-other-element-face ((((background dark)) (:foreground "#ffccff"))))
 '(xsl-xslt-alternate-face ((((background dark)) (:foreground "#ccffcc"))))
 '(xsl-xslt-main-face ((((background dark)) (:foreground "#ccccff"))))
 '(zmacs-region ((t (:foreground "black" :background "#646499"))) t))

(put 'set-goal-column 'disabled nil)
(setq load-home-init-file t) ; don't load init file from ~/.xemacs/init.el


