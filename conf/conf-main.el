(setq load-path (cons "~/Library/Emacs/elisp" load-path))

;(setq load-path (cons "~/Library/Emacs/elisp/cc-mode-5.28" load-path))

;(load "~/Library/Emacs/elisp/nxhtml/autostart.el")

;(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)


(require 'cl)

;(require 'ns-platform-support)
;(ns-extended-platform-support-mode 1)

(prefer-coding-system 'utf-8)

(setq vc-follow-symlinks t)
(setq message-log-max 200)
;(setq-default show-trailing-whitespace t) ;; use M-x delete-trailing-whitespace to kill it
(fset 'yes-or-no-p 'y-or-n-p)

;(add-to-list 'load-path (expand-file-name "~/Library/Emacs/elisp/elib-1.0"))
;(add-to-list 'load-path (expand-file-name "~/Library/Emacs/elisp/jdee-2.4.0.1/lisp"))
;(add-to-list 'load-path (expand-file-name "~/Library/Emacs/elisp/cedet/common"))

;(load-file (expand-file-name "~/Library/Emacs/elisp/cedet/common/cedet.el"))
;(global-ede-mode t)
;(semantic-load-enable-code-helpers)
;(global-srecode-minor-mode 1)
;(require 'jde)

(require 'font-lock)
(require 'delsel)
(require 'css-mode)
(require 'comint)
(require 'generic-x)
(require 'filladapt)
(require 'point-stack)
(require 'hilit-chg)

(require 'dtrt-indent)
(dtrt-indent-mode 1)

;(require 'yaml-mode)
;(require 'protobuf-mode)
;(require 'perl-mode)
;(require 'php-mode)
;(require 'javascript-mode)
;(require 'actionscript-mode)
;(require 'haskell-mode)
;(require 'literate-haskell-mode)


(autoload 'less-css-mode "less-css-mode" "Major mode for editing LESS CSS" t)
(autoload 'yaml-mode "yaml-mode" "Major mode for editing YAML" t)
(autoload 'protobuf-mode "protobuf-mode" "Major mode for editing Protocol Buffers" t)
(autoload 'perl-mode "perl-mode" "Major mode for editing Perl" t)
(autoload 'php-mode "php-mode" "Major mode for editing PHP" t)
(autoload 'javascript-mode "javascript-mode" "Major mode for editing Javascript" t)
(autoload 'actionscript-mode "actionscript-mode" "Major mode for editing ActionScript." t)
(autoload 'haskell-mode "haskell-mode" "Major mode for editing Haskell scripts." t)
(autoload 'literate-haskell-mode "haskell-mode" "Major mode for editing literate Haskell scripts." t)

;; (autoload 'perl-mode "cperl-mode" "alternate mode for editing Perl programs" t)

;(setq mac-command-modifier 'meta)
;(setq mac-function-modifier nil)



;(setq whitespace-modes (cons 'sql-mode (cons 'php-mode whitespace-modes)))

;(setq font-lock-support-mode 'jit-lock-mode)
;(setq-default font-lock-multiline t)
;(setq jit-lock-stealth-time 16
;      jit-lock-defer-contextually t
;      jit-lock-stealth-nice 0.6)


(setq
 backup-by-copying t                    ; don't clobber symlinks
 backup-directory-alist
 '(("." . "~/Library/Emacs/backups"))   ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)                     ; use versioned backups

(setq make-backup-files nil)

;; Haven't found the customise settings for these...
(setq c-recognize-knr-p nil)
(setq minibuffer-max-depth nil)

(c-add-style "gnu4" '("gnu" (c-basic-offset . 4)))
(setq c-default-style "gnu4")

(add-hook 'php-mode-hook (lambda ()
    (defun ywb-php-lineup-arglist-intro (langelem)
      (save-excursion
        (goto-char (cdr langelem))
        (vector (+ (current-column) c-basic-offset))))
    (defun ywb-php-lineup-arglist-close (langelem)
      (save-excursion
        (goto-char (cdr langelem))
        (vector (current-column))))
    (c-set-offset 'arglist-intro 'ywb-php-lineup-arglist-intro)
    (c-set-offset 'arglist-close 'ywb-php-lineup-arglist-close)))



(setq Info-default-directory-list (cons "/usr/share/info/" Info-default-directory-list))



(setq truncate-partial-width-windows nil)



;; Scrolling and mouse wheel

(defun scroll-one-line-up (&optional arg)
  "Scroll the selected window up (forward in the text) one line (or N lines)."
  (interactive "p")
  (scroll-up (or arg 1)))

(defun scroll-one-line-down (&optional arg)
  "Scroll the selected window down (backward in the text) one line (or N)."
  (interactive "p")
  (scroll-down (or arg 1)))

(defun my-scroll-up (&optional arg)
  "Scrolls text up ARG lines keeping cursor at the same (screen) position"
  (interactive "p")
  (scroll-up (or arg 1))
  (next-line (or arg 1)))

(defun my-scroll-down (&optional arg)
  "Scrolls text down ARG lines keeping the cursor at the same (screen) position"
  (interactive "p")
  (scroll-down (or arg 1))
  (previous-line (or arg 1)))

(fset 'up-3 [(control up) (control up) (control up)])
(fset 'down-3 [(control down) (control down) (control down)])

(defun insert-real-tab (&optional arg)
  "Inserts a real tab character"
  (interactive "p")
  (insert "\t")
  )

(fset 'my-up-3 [(control shift meta up) (control shift meta up) (control shift meta up)])
(fset 'my-down-3 [(control shift meta down) (control shift meta down) (control shift meta down)])

(defun gid-twiddle-whitespace (&optional arg)
  "Clean up whitespace."
  (interactive "p")
  (save-excursion
    (save-restriction
      (goto-char (point-min))
      (while (re-search-forward "[\s\t]+$" nil t nil)
        (replace-match "")
        )
      )
    ))
;  (whitespace-cleanup))

(defun tabify-buffer (&optional arg)
  "Tabify entire buffer"
  (interactive "p")
  (tabify (point-min) (point-max))
  (gid-twiddle-whitespace arg))

(defun untabify-buffer (&optional arg)
  "Untabify entire buffer"
  (interactive "p")
  (gid-twiddle-whitespace arg)
  (untabify (point-min) (point-max)))

(defun indent-whole-buffer (&optional arg)
  "Reindent whole buffer"
  (interactive "p")
  (indent-region (point-min) (point-max)))

(setq cssm-indent-function #'cssm-c-style-indenter)



(setq auto-mode-alist
      (append '(
    ("\\.as$" . actionscript-mode)
    ("\\.as3$" . actionscript-mode)
    ("\\.asa$" . perl-mode)
    ("\\.asp$" . html-mode)
    ("\\.c$"  . c-mode)
    ("\\.htm" . html-mode)
    ("\\.pde$" . java-mode)
    ("\\.inc$" . java-mode)
    ("\\.js$" . javascript-mode)
    ("\\.less$" . less-css-mode)
    ("\\.jsp$" . html-mode)
    ("\\.php" . php-mode)
    ("\\.pm$" . perl-mode)
    ("\\.css$" . css-mode)
    ("\\.rcp$" . pilrc-mode)
    ("[Mm]akefile$" . makefile-mode)
    ("\\.cl$" . c-mode)
    ("\\.[HC]$"  . c++-mode)
    ("\\.[hc]\\+\\+$"  . c++-mode)
    ("\\.[hc]pp$"  . c++-mode)
    ("\\.[hg]s$"  . haskell-mode)
    ("\\.d$"  . c-mode)
    ("\\.emacs"  . lisp-mode)
    ("\\.h$"  . c-mode)
    ("\\.hi$"     . haskell-mode)
    ("\\.java$"  . java-mode)
    ("\\.jj$"  . java-mode)
    ("\\.js$" . c++-mode)
    ("\\.l[hg]s$" . literate-haskell-mode)
    ("\\.lb$" . fundamental-mode)
    ("\\.log$"  . fundamental-mode)
    ("\\.mk$"  . makefile-mode)
    ("\\.pl$" . perl-mode)
    ("\\.pov$"  . pov-mode)
    ("\\.tgz$" . tar-mode)
    ("\\.yml$" . yaml-mode)
    ("_emacs"  . lisp-mode)
    ("report" . report-mode)
    ;;                ("\\.y$" . bison-mode)
    ;;                ("\\.yy$" . bison-mode)
    ) auto-mode-alist) )




;; Clock
(display-time)
;(setq display-time-mail-sign nil)

;; Filladapt
;;(defun my-filladapt-c-mode-common-hook ()
;;  (c-setup-filladapt)
;;  (filladapt-mode 1))
;;(add-hook 'c-mode-common-hook 'my-filladapt-c-mode-common-hook)



;;
;; with `haskell-mode.el' accessible somewhere on the load-path.
;; To add a directory `~/Library/Emacs/emacs' (for example) to the load-path,
;; add the following to .emacs:
;;
;;    (setq load-path (cons "~/Library/Emacs/emacs" load-path))
;;
;; To turn any of the supported modules on for all buffers, add the
;; appropriate line(s) to .emacs:
;;

(add-hook 'haskell-mode-hook 'turn-on-haskell-font-lock)
(add-hook 'haskell-mode-hook 'turn-on-haskell-decl-scan)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;;    (add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)
(add-hook 'haskell-mode-hook 'turn-on-haskell-hugs)

(load-library "conf-skeleton")

(global-font-lock-mode t)

(load-library "conf-bindings")

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(require 'whitespace)
(global-whitespace-mode t)


;; Default to use spaces rather than tabs in indention
(setq-default indent-tabs-mode nil);;

;; (autoload 'smart-tabs-mode "smart-tabs-mode" "Intelligently indent with tabs, align with spaces!")
;; (autoload 'smart-tabs-mode-enable "smart-tabs-mode")
;; (autoload 'smart-tabs-advice "smart-tabs-mode")
;;
;; ;(smart-tabs-insinuate 'c 'javascript 'cperl 'python 'ruby 'php)
;;
;; (add-hook 'c-mode-hook 'smart-tabs-mode-enable)
;; (smart-tabs-advice c-indent-line c-basic-offset)
;; (smart-tabs-advice c-indent-region c-basic-offset)
;;
;; (add-hook 'js2-mode-hook 'smart-tabs-mode-enable)
;; (smart-tabs-advice js2-indent-line js2-basic-offset)
;;
;; (add-hook 'cperl-mode-hook 'smart-tabs-mode-enable)
;; (smart-tabs-advice cperl-indent-line cperl-indent-level)
;;
;; (add-hook 'python-mode-hook 'smart-tabs-mode-enable)
;; (smart-tabs-advice python-indent-line-1 python-indent)
;;
;; (add-hook 'ruby-mode-hook 'smart-tabs-mode-enable)
;; (smart-tabs-advice ruby-indent-line ruby-indent-level)
;;
;; (add-hook 'php-mode-hook 'smart-tabs-mode-enable)
;; (smart-tabs-advice php-indent-line php-indent-level)

(add-to-list 'load-path "~/Library/Emacs/elisp/jshint-mode")
(require 'flymake-jshint)
(add-hook 'javascript-mode-hook
          (lambda () (flymake-mode t)))


(defun gid-php-mode-fn ()
  "Function to run when php-mode is initialized for a buffer."
  (setq c-default-style "bsd" c-basic-offset 4)

  ;; not sure if necessary or not.
  (modify-syntax-entry ?/ ". 124b" php-mode-syntax-table)
  (modify-syntax-entry ?* ". 23" php-mode-syntax-table)
  (modify-syntax-entry ?\n "> b"  php-mode-syntax-table)
  (modify-syntax-entry ?\^m "> b" php-mode-syntax-table)

  (setq comment-multi-line nil ;; maybe
        comment-start "// "
        comment-end ""
        comment-style 'indent
        comment-use-syntax t)


  (fset 'joomla-insert-mark
        (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([tab 74 80 114 111 102 105 108 101 114 58 58 103 101 116 73 110 115 116 97 110 99 101 40 39 65 112 112 108 105 99 97 116 105 111 110 39 41 45 62 109 97 114 107 40 39 39 41 59 13 tab 1 18 39 39 right] 0 "%d")) arg)))

  (fset 'joomla-insert-mark-line
        [?\C-k ?\C-k ?\C-y ?\C-y up up tab ?J ?P ?r ?o ?f ?i ?l ?e ?r ?: ?: ?g ?e ?t ?I ?n ?s ?t ?a ?n ?c ?e ?\( ?\' ?A ?p ?p ?l ?i ?c ?a ?t ?i ?o ?n ?\' ?\) ?- ?. ?M backspace backspace ?> ?m ?a ?r ?k ?\( ?\' ?\C-  ?\C-e ?\M-x ?r ?e ?p ?l ?a ?c ?e ?- ?s ?t ?r ?i ?n ?g return ?\' return return ?\C-e ?\' ?\) ?\; right down])

  (local-set-key [f14] 'joomla-insert-mark)
  (local-set-key [(control f14)] 'joomla-insert-mark-line)
  )


(add-hook 'php-mode-hook 'gid-php-mode-fn)
