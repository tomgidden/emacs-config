(require 'zoom-frm)
(define-key (current-global-map) [(meta ?+)] 'zoom-in/out)
(define-key (current-global-map) [(meta ?-)] 'zoom-in/out)
(define-key (current-global-map) [(meta ?=)] 'zoom-in/out)
(define-key (current-global-map) [(meta ?0)] 'zoom-in/out)
(define-key (current-global-map) [(meta ?])] 'toggle-frame-maximized)

(global-set-key "\C-\\" 'kill-rectangle)
(global-set-key "\C-x\C-k" 'kill-buffer)
(global-set-key "\C-x\C-z" nil)
(global-set-key "\M-l" 'goto-line)

(global-set-key [(hyper \%)] 'replace-string)

(global-set-key [kp-delete] 'delete-char)
(global-set-key [M-kp-delete] 'kill-word)

(global-set-key [(control \+)] 'redo)
(global-set-key [(control backspace)]  'backward-kill-word)
(global-set-key [(control delete)]     'forward-kill-word)
(global-set-key [(control end)]  'end-of-buffer)
(global-set-key [(control home)] 'beginning-of-buffer)
(global-set-key [(control kp-end)]  'end-of-buffer)
(global-set-key [(control kp-home)] 'beginning-of-buffer)
(global-set-key [(control left)]       'backward-word)

(global-set-key [(control down)] 'scroll-one-line-up)
(global-set-key [(control mouse-4)] 'scroll-down)
(global-set-key [(control mouse-5)] 'scroll-up)
(global-set-key [(control up)] 'scroll-one-line-down)

(global-set-key [(control next)]  'end-of-buffer)
(global-set-key [(control prior)] 'beginning-of-buffer)
(global-set-key [(control right)]      'forward-word)
(global-set-key [(control shift f12)] 'describe-face-at-point)
(global-set-key [(control shift meta down)] 'my-scroll-down)
(global-set-key [(control shift meta up)] 'my-scroll-up)
(global-set-key [(control tab)] 'insert-real-tab)

(global-set-key [(meta next)]  'end-of-buffer-other-window)
(global-set-key [(meta prior)] 'beginning-of-buffer-other-window)

(global-set-key [\M-\C-q] 'justify-current-line)
(global-set-key [end]    'end-of-line)

(global-set-key [f4] 'point-stack-push)
(global-set-key [(control f4)] 'point-stack-push)

(global-set-key [f5] 'point-stack-pop)
(global-set-key [(control f5)] 'point-stack-pop)

(global-set-key [f6] 'untabify-buffer)
(global-set-key [(control f6)] 'untabify-buffer)

(global-set-key [f7] 'tabify-buffer)
(global-set-key [(control f7)] 'tabify-buffer)

(global-set-key [f8] 'blank-cleanup)
(global-set-key [(control f8)] 'blank-cleanup)

(global-set-key [f9] 'delete-indentation)
(global-set-key [(control f9)] 'delete-indentation)

(global-set-key [f10] 'indent-whole-buffer)
(global-set-key [(control f10)] 'indent-whole-buffer)

(global-set-key [f11] 'call-last-kbd-macro)
(global-set-key [(control f11)] 'call-last-kbd-macro)

(global-set-key [f12] 'gid-twiddle-whitespace)
(global-set-key [(control f12)] 'gid-twiddle-whitespace)

(global-set-key [f13] 'font-lock-fontify-buffer)
(global-set-key [(control f13)] 'font-lock-fontify-buffer)

(global-set-key [home]   'beginning-of-line)

(global-set-key [mouse-4] 'my-down-3)
(global-set-key [mouse-5] 'my-up-3)

(global-set-key [help] 'overwrite-mode)

;(require 'bork)

;; Define the return key to avoid problems on MacOS X
(define-key function-key-map [return] [13])

;; Remove the stupid treatment of closing braces in CSS Mode
;(define-key cssm-mode-map (read-kbd-macro "}") nil)

(global-set-key [?\H-3] "#")


(global-set-key [(hyper h)] 'html-mode)
(global-set-key [(hyper p)] 'php-mode)
(global-set-key [(hyper j)] 'javascript-mode)
(global-set-key [(hyper c)] 'css-mode)



;(global-set-key [(alt a)] 'mark-whole-buffer)
;(global-set-key [(alt v)] 'yank)
;(global-set-key [(alt c)] 'kill-ring-save)
;(global-set-key [(alt x)] 'kill-region)
;(global-set-key [(alt s)] 'save-buffer)
;(global-set-key [(alt l)] 'goto-line)
;(global-set-key [(alt o)] 'find-file)
;(global-set-key [(alt f)] 'isearch-forward)
;(global-set-key [(alt g)] 'isearch-repeat-forward)
;(global-set-key [(alt w)] (lambda () (interactive) (kill-buffer (current-buffer))))
;(global-set-key [(alt .)] 'keyboard-quit)
;(global-set-key [(alt q)] 'save-buffers-kill-emacs)

;(require 'redo)
;(global-set-key [(alt z)] 'undo)
;(global-set-key [(alt shift z)] 'redo)
