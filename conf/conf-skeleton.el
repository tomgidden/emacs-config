
; (require 'skeleton)
; (define-skeleton h4-skel
;   "enclose selected text in an h4 tag"
;   nil
;   > "<h4>"
;   > _
;   "</h4>" >)
; (define-skeleton i-skel
;   "enclose selected text in an italic tag"
;   nil
;   > "<i>"
;   > _
;   "</i>" >)
; (define-skeleton em-skel
;   "enclose selected text in an emphasised tag"
;   nil
;   > "<em>"
;   > _
;   "</em>" >)
; (define-skeleton p-skel
;   "enclose selected text in a p tag"
;   nil
;   > "<p>"
;   > _
;   "</p>" >)
; 
; (global-set-key [f5] 'h4-skel)
; (global-set-key [f6] 'i-skel)
; (global-set-key [f7] 'p-skel)
; (global-set-key [f8] 'em-skel)
